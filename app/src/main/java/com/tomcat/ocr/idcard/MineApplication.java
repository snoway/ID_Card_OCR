package com.tomcat.ocr.idcard;

import android.app.Application;

import com.msd.ocr.idcard.LibraryInitOCR;

public class MineApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        LibraryInitOCR.initOCR(this);
    }

}
